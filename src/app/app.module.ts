import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { TerminalPage } from '../pages/terminal/terminal';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {PadPage} from "../pages/pad/pad";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { GlobalProvider } from '../providers/global/global';
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope';



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    TerminalPage,
    HomePage,
    TabsPage,
    PadPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    TerminalPage,
    HomePage,
    TabsPage,
    PadPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BluetoothSerial,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    Gyroscope,
  ]
})
export class AppModule {}
