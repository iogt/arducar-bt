import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import {GlobalProvider} from "../../providers/global/global";
import {Platform} from "ionic-angular";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope';

@Component({
  selector: 'page-pad',
  templateUrl: 'pad.html'
})
export class PadPage {
  btcmd = ""
  history =""
  isConnected = false
  arm_horizontal = 90
  arm_vertical = 90
  private bt;
  gyro_x =0;
  gyro_y =0;
  gyro_z =0;
  constructor(
    public navCtrl: NavController,
    private bluetoothSerial: BluetoothSerial,
    public global: GlobalProvider,
    private screenOrientation: ScreenOrientation,
    private gyroscope: Gyroscope,
    public plt: Platform) {
    this.bt = bluetoothSerial;


    let options: GyroscopeOptions = {
      frequency: 500
    };

    plt.ready().then((readySource) => {

      if (readySource == 'cordova') {
        this.gyroscope.watch(options)
          .subscribe((orientation: GyroscopeOrientation) => {
            if(this.global.useGyro){
              console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
              this.gyro_x = parseFloat(orientation.x.toFixed(4));
              this.gyro_y = parseFloat(orientation.y.toFixed(4));
              this.gyro_z = parseFloat(orientation.z.toFixed(4));
            }
          });
      }
      else {
        this.gyro_x = -1;
        this.gyro_y = -1;
        this.gyro_z = -1;
      }
    });

  }


  ionViewWillEnter(){
    // set to landscape
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then( (res)=>{
    //   console.log(res)
    // });

    this.bluetoothSerial.isConnected().then(res => {
      this.history = "Connection Success"
      this.btcmd = ""
    }).catch(res => {
      this.history = "$ Error! Not conected to "+this.global.bt_address
      console.log(res);
    });

    this.bluetoothSerial.subscribe('\n').subscribe((data)=>{
      console.log(data)
      this.history += '\n'+data
    }, (err)=>{
      console.log(err)
      this.history += '\n'+err
    })
    //   .then( (data)=>{
    //   console.log(data)
    // })
  }
  changeHorizontal(evt){
    console.log("Horizontal",this.arm_horizontal)
    this.btnSend(evt, this.arm_horizontal)
  }
  changeVertical(evt){
    console.log("Vertical", this.arm_vertical+200)
    this.btnSend(evt, this.arm_vertical+200)
  }
  btnSend(evt, cmd){
    this.btcmd = cmd
    console.log("Sending action",cmd)
    this.send2TB(evt)
  }
  send2TB(evt){
    if(this.global.isConnected){
      this.btcmd =this.btcmd.toString().toLocaleLowerCase()
      this.bluetoothSerial.isConnected().then(res => {
        this.history = "$ "+this.btcmd.toLocaleLowerCase()
        this.bluetoothSerial.write(this.btcmd).then((res)=>{
          console.log(res)
        }, (err)=>{
          console.log(err)
        });
        this.btcmd = ""
      }).catch(res => {
        this.history = "$ Error! Not conected"
        console.log(res);
      });
    }

  }
  keyDownFunction(event){
    if(event.keyCode == 13) {
      this.send2TB(event);
    }
  }
}
