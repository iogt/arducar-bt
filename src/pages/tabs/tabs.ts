import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { TerminalPage } from '../terminal/terminal';
import { HomePage } from '../home/home';
import {PadPage} from "../pad/pad";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TerminalPage;
  tab3Root = AboutPage;
  tabRC = PadPage;

  constructor() {

  }
}
