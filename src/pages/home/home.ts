import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {BluetoothSerial} from "@ionic-native/bluetooth-serial";
import { GlobalProvider } from "../../providers/global/global";
import {ScreenOrientation} from "@ionic-native/screen-orientation";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  bt_device = null
  bt_status = false
  bt_list = []
  bt_text_status = ""
  constructor(public navCtrl: NavController, private bluetoothSerial: BluetoothSerial,public alertCtrl: AlertController,public global: GlobalProvider,private screenOrientation: ScreenOrientation) {
    bluetoothSerial.isEnabled().then(() => {
      console.log("bluetooth is enabled all G");
      this.search()
    }, () => {
      console.log("bluetooth is not enabled trying to enable it");
      bluetoothSerial.enable().then(() => {
        console.log("bluetooth got enabled hurray");
        this.search()
      }, () => {
        console.log("user did not enabled");
      })
    });
  }
  ionViewWillEnter(){
    this.screenOrientation.unlock();
  }
  showInfo(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons: ['OK']
    });
    alert.present();
  }
  showError(info) {
    let alert = this.alertCtrl.create({
      title: "Error",
      message: info,
      buttons: ['OK']
    });
    alert.present();
}

  connect(){
    console.log("Connecting to ",this.global.bt_address)
    this.bt_text_status = "Conectando..."
    this.bt_device = this.global.bt_address
    this.bluetoothSerial.connectInsecure(this.global.bt_address).subscribe((data) => {
      console.log("Connected", data);
      this.bt_text_status = "Conectado!"
      this.bt_status = true
      this.global.isConnected = true;
      this.showInfo("Fuck Yea!", "Conectado correctamente a "+this.global.bt_address)
    }, (error) => {
      console.log("not Connected", error);
      this.bt_text_status = "Error al conectar"
      this.showError("Error al conectar")
      this.global.isConnected = false;
      this.bt_status = false
    });
  }
  disconect(){
    this.bluetoothSerial.disconnect().then((res)=>{
      console.log("Disconected", res)
      this.bt_text_status = ""
      this.global.isConnected = false;
      this.bt_status = false
    });
  }
  search(){
    this.bluetoothSerial.list().then((data) => {
        console.log(JSON.stringify( data ));
        console.log("hurray",data);
        this.bt_list = data;
        // this.showDevices("Found Devices", data.id, data.class, data.address, data.name);
      },
      (error) => {
        console.log("could not find paired devices because: " + error);
        this.showError(error);
      });
  }
}
