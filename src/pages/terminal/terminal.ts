import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import {GlobalProvider} from "../../providers/global/global";
import {global} from "@angular/core/src/util";
import {ScreenOrientation} from "@ionic-native/screen-orientation";

@Component({
  selector: 'page-terminal',
  templateUrl: 'terminal.html'
})
export class TerminalPage {
  btcmd = ""
  history =""
  isConnected = false
  private bt
  constructor(public navCtrl: NavController,private bluetoothSerial: BluetoothSerial,public global: GlobalProvider,private screenOrientation: ScreenOrientation) {
    this.bt = bluetoothSerial;
  }
  ionViewWillEnter(){
    this.screenOrientation.unlock();
    this.bluetoothSerial.isConnected().then(res => {
      this.history += "\nConnection Success"
      this.btcmd = ""
    }).catch(res => {
      this.history += "\n$ Error! Not conected to "+this.global.bt_address
      console.log(res);
    });

    this.bluetoothSerial.subscribe('\n').subscribe((data)=>{
      console.log(data)
      this.history += '\n'+data
    }, (err)=>{
      console.log(err)
    })
    //   .then( (data)=>{
    //   console.log(data)
    // })
  }
  send2TB(evt){
    if(this.global.isConnected){
      this.btcmd =this.btcmd.toLocaleLowerCase()
      this.bluetoothSerial.isConnected().then(res => {
        this.history += "\n$ "+this.btcmd.toLocaleLowerCase()
        this.bluetoothSerial.write(this.btcmd).then((res)=>{
          console.log(res)
        }, (err)=>{
          console.log(err)
        });
        this.btcmd = ""
      }).catch(res => {
        this.history += "\n$ Error! Not conected"
        console.log(res);
      });
    }

  }
  keyDownFunction(event){
    if(event.keyCode == 13) {
      this.send2TB(event);
    }
  }
}
